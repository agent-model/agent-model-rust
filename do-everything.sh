#!/usr/bin/env bash
./process.sh
(cd ./hospital-sizes && ./do-everything.sh)
(cd ./single-patient && ./do-everything.sh)
(cd ./synthetic && ./do-everything.sh)