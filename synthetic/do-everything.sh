#!/usr/bin/env bash
./do-one.sh 2 5  100 0.03 fast-low & \
./do-one.sh 2 5  100 0.97 fast-high & \
./do-one.sh 2 5  100 0.5  fast-middle & \
./do-one.sh 7 30  100 0.03 first-low & \
./do-one.sh 7 30  100 0.97 first-high & \
./do-one.sh 7 30  100 0.5  first-middle & \
./do-one.sh 7 365 100 0.03 second-low & \
./do-one.sh 7 365 100 0.97 second-high & \
./do-one.sh 7 365 100 0.5  second-middle
