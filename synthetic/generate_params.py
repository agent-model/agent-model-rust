import json
import sys

beta_inv = int(sys.argv[1])
gamma_inv = int(sys.argv[2])
steps = int(sys.argv[3])
ratio = float(sys.argv[4])
count = int(sys.argv[5])
patients = int(sys.argv[6])

print(json.dumps([{
    "beta_inv": beta_inv,
    "gamma_inv": gamma_inv,
    "ratio": ratio,
    "seed": i,
    "steps": steps,
    "patients": patients,
    "output": "seed=" + str(i),
} for i in range(count)]))
