#!/usr/bin/gnuplot -c
dataname = "./plots/".ARG5.".txt"
imgname = "./plots/".ARG5.".png"
set terminal pngcairo enhanced font "arial,30" fontscale 1.0 size 1600,1000;
set output imgname
set xrange [0:ARG3]
set yrange [0:1]
b = 1.0/(ARG1*2.0)
g = 1.0/(ARG2*2.0)
r = b - g
i = ARG4
g(x) = exp(-r*x)
f(x) = r/(b*(1-g(x))+(r*g(x)/i))
set xlabel "Simulation step"
set ylabel "Infected fraction"
set title "Discrete vs continuous simulation"
set key on
set key right bottom
plot for [i=1:300] dataname u i lc rgb "#AAFF0000" lw 1 with lines notitle, \
                   f(x) with lines lc rgb "green" lw 3 title "continuous", \
                   10000 lc rgb "red" lw 3 with lines title "discrete"

