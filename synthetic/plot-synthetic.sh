#!/usr/bin/env bash
mkdir -p ./results/
mkdir -p ./plots/
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_synth -- "./results/$1.zip" > "./plots/$1.txt"
