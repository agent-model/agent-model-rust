#!/usr/bin/env bash
mkdir -p ./results/
mkdir -p ./plots/
python3 generate_params.py $1 $2 $3 $4 300 200 | \
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin simulate_synth -- "./results/$5.zip"
