#!/usr/bin/env bash
zipname=./results/results.zip
processed=../data/processed.json
txtname=./plots/results.txt
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_continuous_approx -- \
--input ${zipname}                                                                   \
--processed ${processed}                                                             \
--output ${txtname}
