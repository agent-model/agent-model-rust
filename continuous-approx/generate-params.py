import json
import sys

count = int(sys.argv[1])
beta_inv_min = int(sys.argv[2])
beta_inv_max = int(sys.argv[3])
beta_inv_step = int(sys.argv[4])
gamma_inv_min = int(sys.argv[5])
gamma_inv_max = int(sys.argv[6])
gamma_inv_step = int(sys.argv[7])

params = []
for b in range(beta_inv_min, beta_inv_max, beta_inv_step):
    for g in range(gamma_inv_min, gamma_inv_max, gamma_inv_step):
        for i in range(count):
            params.append({
                "beta_inv": b,
                "gamma_inv": g,
                "step": 200,
                "seed": i,
                "output": json.dumps([b, g, i]),
                "simtype": {
                    "RandomRatio": {
                        "ratio": 0.3
                    }
                }
            })
print(json.dumps(params))