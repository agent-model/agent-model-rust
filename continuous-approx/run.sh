#!/usr/bin/env bash
mkdir -p ./results/
mkdir -p ./plots/
zipname=./results/results.zip
processed=../data/processed.json
python3 ./generate-params.py 5 5 15 2 30 300 45 | \
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin simulate_any -- ${processed} ${zipname}
