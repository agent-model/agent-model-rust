import json
import sys

count = int(sys.argv[1])
beta_inv = int(sys.argv[2])
gamma_inv = int(sys.argv[3])
hospital = int(sys.argv[4])

print(json.dumps([{
    "beta_inv": beta_inv,
    "gamma_inv": gamma_inv,
    "step": 200,
    "seed": i,
    "output": "seed=" + str(i),
    "simtype": {
        "FirstPatient": {
            "hospital": hospital
        }
    }
} for i in range(count)]))
