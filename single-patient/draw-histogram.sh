#!/usr/bin/gnuplot -c
dataname = './plots/histogram-'.ARG4.'.txt'
imgname = "./plots/histogram-".ARG4.".png"
set terminal pngcairo linewidth 3 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output imgname
set autoscale xfix
set style histogram rowstacked gap 0
set style fill solid 0.5 border lt -1
set boxwidth 10000
plot dataname smooth freq with boxes