#!/usr/bin/env bash
zipname=./results/$1.zip
txtname=./plots/pzero-$1.txt
processed=../../data/processed.json
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_patient_zero_prob -- \
--input ${zipname}                                 \
--processed ${processed}                           \
--output ${txtname}