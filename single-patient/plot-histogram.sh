#!/usr/bin/env bash
tmpname=`mktemp`
zipname=./results/$1.zip
txtname=./plots/histogram-$1.txt
processed=../../data/processed.json
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_infection_histograms -- \
--input ${zipname}                                     \
--processed ${processed}                               \
--output ${tmpname}
maphimbu -d 10000 -C < ${tmpname} > ${txtname}
sort -g -r -k1,1 ${txtname} -o ${txtname}
rm ${tmpname}
