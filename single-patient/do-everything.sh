#!/usr/bin/env bash
mkdir -p params1
mkdir -p params2
mkdir -p params3
mkdir -p params4
mkdir -p params5
ARGS=`cut -d' ' -f7 < ../hospital-sizes/plots/hospitals-stats.txt | awk 'NR % 25 == 10'`
ARGS2=`cut -d' ' -f1,7 < ../hospital-sizes/plots/hospitals-stats.txt | awk 'NR % 25 == 10'`
cd ./params1
#echo "$ARGS" | xargs -n 1 -P 2  ../run.sh 7 365
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-curve.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-cumulant.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-sis.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-p-zero.sh
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-curve.sh 7 365 180000
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-cumulant.sh 7 365
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-sis.sh 7 365
cd ../
cd ./params2
#echo "$ARGS" | xargs -n 1 -P 2 ../run.sh 7 30
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-curve.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-cumulant.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-sis.sh
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-curve.sh 7 30 8000
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-cumulant.sh 7 30
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-sis.sh 7 30
cd ../
cd ./params3
#echo "$ARGS" | xargs -n 1 -P 2 ../run.sh 12 365
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-curve.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-cumulant.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-sis.sh
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-curve.sh 12 365 100000
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-cumulant.sh 12 365
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-sis.sh 12 365
cd ../
cd ./params4
#echo "$ARGS" | xargs -n 1 -P 2 ../run.sh 12 30
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-curve.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-cumulant.sh
#echo "$ARGS" | xargs -n 1 -P 10 ../plot-sis.sh
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-curve.sh 12 30 80
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-cumulant.sh 12 30
#echo "$ARGS2" | xargs -n 2 -P 10 ../draw-sis.sh 12 30
cd ../
ARGS=139
ARGS2="209 139"
cd ./params5
#echo "$ARGS" | xargs -n 1 -P 10 python3 ../cond-prob.py
echo "$ARGS2" | xargs -n 2 -P 10 ../draw-curve.sh 7 365 180000
echo "$ARGS2" | xargs -n 2 -P 10 ../draw-prob.sh 7 365
cd ../