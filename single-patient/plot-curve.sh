#!/usr/bin/env bash
zipname=./results/$1.zip
txtname=./plots/curve-$1.txt
processed=../../data/processed.json
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_infection_curves -- \
--input ${zipname}                                 \
--processed ${processed}                           \
--output ${txtname}