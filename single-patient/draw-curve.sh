#!/usr/bin/gnuplot -c
dataname = './plots/curve-'.ARG5.'.txt'
imgname = "./plots/curve-".ARG5.".png"
set terminal pngcairo linewidth 0.1 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output imgname
set xrange [200:5644]
set yrange [0:ARG3]
set style fill transparent solid 0.001
set xlabel "Simulation step"
set ylabel "Number of infected agents"
b = ARG1
g = ARG2
set title sprintf("Population no. %i, 1/{/Symbol b} = %s days, 1/{/Symbol g} = %s days",ARG4 + 1,b,g)
plot for [col=2:1001] dataname using col with lines lc rgb "red" notitle, \
     for [col=2:1001] dataname using col with filledcurves x1 lc rgb "red" notitle
