#!/usr/bin/env bash
mkdir -p ./results/
mkdir -p ./plots/
zipname=./results/$3.zip
txtname=./plots/single-$3.txt
processed=../../data/processed.json
python3 ../generate-params.py 1000 $1 $2 $3 | \
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin simulate_any -- ${processed} ${zipname}
