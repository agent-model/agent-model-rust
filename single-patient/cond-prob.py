import sys
lines = []
data = "./plots/pzero-" + sys.argv[1] + ".txt"
out = "./plots/pzero-prob-" + sys.argv[1] + ".txt"
with open(data) as f, open(out, "w") as o:
    for l in f:
        time, endemic = l.split()
        lines.append((int(time), int(endemic)))
    maxtime = max(time for (time, endemic) in lines)
    for x in lines:
        print(x)
    for i in range(maxtime):
        all_sims = sum(1 for (time, endemic) in lines if time > i)
        endemic = sum(endemic for (time, endemic) in lines if time > i)
        print(float(endemic)/float(all_sims), all_sims - endemic, all_sims, file=o)
