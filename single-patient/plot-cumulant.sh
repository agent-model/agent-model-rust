#!/usr/bin/env bash
zipname=./results/$1.zip
txtname=./plots/cumulant-$1.txt
processed=../../data/processed.json
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_infection_cumulants -- \
--input ${zipname}                                    \
--processed ${processed}                              \
--output ${txtname}