#!/usr/bin/gnuplot -c
dataname = './plots/pzero-prob-'.ARG4.'.txt'
imgname = "./plots/pzero-prob-".ARG4.".png"
set terminal pngcairo linewidth 2 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output imgname
set yrange [0:1]
set y2range [0:1000]
set xrange [0:5444]
set style fill transparent solid 0.001
set y2tics 100
set key right bottom
set xlabel "Number of steps"
set ylabel "Probability of endemic state"
set y2label "Simulations with patient \"0\" infected"
b = ARG1
g = ARG2
set title sprintf("Population no. %i, 1/{/Symbol b} = %s days, 1/{/Symbol g} = %s days",ARG3 + 1,b,g)
plot dataname using 1 with lines lc rgb "red" title "probability of endemic state" axes x1y1, \
     dataname using 2 with lines lc rgb "blue" title "non-endemic simulations" axes x1y2, \
     dataname using 3 with lines lc rgb "green" title "all simulations" axes x1y2
