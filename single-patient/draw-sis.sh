#!/usr/bin/gnuplot -c
dataname = './plots/sis-'.ARG4.'.txt'
imgname = "./plots/sis-".ARG4.".png"
set terminal pngcairo linewidth 1 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output imgname
set xrange [200:5644]
set yrange [0:1]
set style fill transparent solid 0.01
set xlabel "Simulation step"
set ylabel "Fraction of infected patients"
b = ARG1
g = ARG2
f(x) = 1.0 - (b * 1.0 / g)
set title sprintf("Population no. %s, 1/{/Symbol b} = %s days, 1/{/Symbol g} = %s days",ARG3,b,g)
plot for [col=2:101] dataname using col with lines lc rgb "red" notitle, \
     for [col=2:101] dataname using col with filledcurves x1 lc rgb "red" notitle, \
     f(x) with lines lw 2
