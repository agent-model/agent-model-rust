#!/usr/bin/gnuplot -c
dataname = './plots/cumulant-'.ARG4.'.txt'
imgname = "./plots/cumulant-".ARG4.".png"
set terminal pngcairo linewidth 3 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output imgname
set xrange [200:5644]
set yrange [0:*]
set style fill transparent solid 1 noborder
set ytics 10
set xlabel "Simulation step"
set ylabel "Number of simulations"
b = ARG1
g = ARG2
set title sprintf("Population no. %i, 1/{/Symbol b} = %s days, 1/{/Symbol g} = %s days",ARG3 + 1,b,g)
plot dataname with lines notitle
