extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate flate2;
extern crate rayon;
extern crate serde_json;
extern crate tar;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::clean_data::HospitalOrd;
use agent_model::clean_data::Hospitals;
use agent_model::clean_data::PatientOrd;
use agent_model::replay::SimulationOutput;
use agent_model::simulation::global_infection_state;
use agent_model::simulation::Simulation;
use clap::App;
use rand::Isaac64Rng;
use rand::Rng;
use rand::SeedableRng;
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use std::fs::read_to_string;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::sync::Mutex;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SimParams {
    #[serde(flatten)]
    common: Common,
    simtype: SimType,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Common {
    beta_inv: f32,
    gamma_inv: f32,
    seed: u32,
    step: usize,
    output: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum SimType {
    RandomRatio { ratio: f32 },
    RandomPatient { hospital: HospitalOrd },
    FirstPatient { hospital: HospitalOrd },
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let processed = read_to_string(&args[1]).unwrap();
    let input: CleanData = serde_json::from_str(&processed).unwrap();

    let stdin = std::io::stdin();
    let params: Vec<SimParams> = serde_json::from_reader(BufReader::new(stdin.lock())).unwrap();

    let out = BufWriter::new(File::create(&args[2]).unwrap());
    let mut output = Mutex::new(zip::ZipWriter::new(out));
    let default_opts = zip::write::FileOptions::default();

    let sims = params
        .into_par_iter()
        .filter_map(|p| run_simulation(&p, &input).map(|o| (p, o)))
        .for_each(|(params, sim)| {
            println!("simulating {:#?}", params);
            let mut lock = &mut *output.lock().unwrap();
            lock.start_file(params.common.output, default_opts).unwrap();
            serde_json::to_writer(&mut lock, &sim).unwrap();
        });
}

fn run_simulation(params: &SimParams, data: &CleanData) -> Option<SimulationOutput> {
    let beta = 1.0 / (params.common.beta_inv * 2.0);
    let gamma = 1.0 / (params.common.gamma_inv * 2.0);
    let step = params.common.step;
    let random = Isaac64Rng::from_seed(&[
        12555,
        0,
        (4842728 as u64).wrapping_add(params.common.seed as u64),
    ]);

    let patients_count = data.patients().len();
    let initial_state = vec![false; data.patients().len()];
    let mut simulation = Simulation::new(initial_state, gamma, beta, random);
    let mut all_healthy = true;

    let mut result = SimulationOutput {
        initial_state: simulation.infection_table().to_vec(),
        changes: Vec::new(),
    };

    let mut steps = data.steps().iter().enumerate().peekable();
    for _ in 0..step {
        let (i, day) = steps.next().unwrap();
        let changes = do_step(&mut simulation, day, &mut all_healthy, i);
        result.changes.push(changes);
    }

    let changes = match params.simtype {
        SimType::RandomRatio { ratio } => {
            let state = global_infection_state(patients_count, ratio, simulation.random());
            let changes = simulation.force_state(state);
            changes
        }
        SimType::RandomPatient { hospital } => {
            let (_, next_day) = steps.peek().unwrap();
            let chosen_hospital = &next_day[hospital];
            let chosen_patient = *simulation.random().choose(chosen_hospital)?;
            let changes = vec![chosen_patient];
            let changes = simulation.force_changes(changes);
            changes
        }
        SimType::FirstPatient { hospital } => {
            let (_, next_day) = steps.peek().unwrap();
            let chosen_hospital = &next_day[hospital];
            let chosen_patient = *chosen_hospital.get(0)?;
            let changes = vec![chosen_patient];
            let changes = simulation.force_changes(changes);
            changes
        }
    };

    if let Some(last_changes) = result.changes.last_mut() {
        assert_eq!(last_changes.len(), 0);
        *last_changes = changes;
    } else {
        for patient in changes {
            result.initial_state[patient] = true;
        }
    }

    all_healthy = false;
    for (i, day) in steps {
        let changes = do_step(&mut simulation, day, &mut all_healthy, i);
        result.changes.push(changes);
    }

    Some(result)
}

fn do_step(
    sim: &mut Simulation,
    day: &Hospitals,
    all_healthy: &mut bool,
    i: usize,
) -> Vec<PatientOrd> {
    if !*all_healthy {
        let mut changes = sim.step(day);
        changes.sort();
        if i % 10 == 0 {
            if sim.infection_table().iter().all(|i| !*i) {
                *all_healthy = true;
            }
        }
        changes
    } else {
        Vec::new()
    }
}
