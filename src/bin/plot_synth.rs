extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::replay::SimulationOutput;
use agent_model::stats::reduce_step_data;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let input = BufReader::new(File::open(&args[1]).unwrap());
    let mut input = zip::ZipArchive::new(input).unwrap();
    let mut rows = vec![];
    for index in 0..input.len() {
        let input = input.by_index(index).unwrap();
        let input = std::io::BufReader::new(input);
        let input = serde_json::from_reader::<_, Vec<f32>>(input).unwrap();

        if index == 0 {
            rows = vec![Vec::new(); input.len()]
        }

        input
            .into_iter()
            .enumerate()
            .for_each(|(i, val)| rows[i].push(val.to_string()));
    }

    let stdout = std::io::stdout();
    for row in rows.into_iter() {
        writeln!(stdout.lock(), "{}", row.join(" ")).unwrap();
    }
}
