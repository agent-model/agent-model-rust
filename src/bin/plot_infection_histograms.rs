extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::replay::SimulationOutput;
use agent_model::stats::reduce_step_data;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    input_fname: String,
    processed_fname: String,
    output_fname: String,
}

struct Io {
    input: BufReader<File>,
    processed: BufReader<File>,
    output: BufWriter<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plot number of infected people at the end of simulation")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(input_arg())
        .arg(output_arg())
        .arg(processed_arg())
        .get_matches();

    let input = get_input(&matches)?;
    let processed = get_processed(&matches)?;
    let output = get_output(&matches)?;
    let input_fname = get_input_fname(&matches);
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            input_fname,
            processed_fname,
            output_fname,
        },
        Io {
            input,
            processed,
            output,
        },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);
    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    let mut counts = Vec::new();

    let mut input = zip::ZipArchive::new(io.input).unwrap();
    for index in 0..input.len() {
        let input = std::io::BufReader::new(input.by_index(index).unwrap());
        let propagation: SimulationOutput = serde_json::from_reader(input).unwrap();
        let mut replay = propagation.replay();
        let steps = propagation.changes.len() - 200;
        for _ in 0..steps {
            replay.next();
        }
        counts.push(replay.status().iter().filter(|i| **i).count());
    }

    for count in counts {
        writeln!(io.output, "{}", count).unwrap();
    }
}
