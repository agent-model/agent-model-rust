extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate serde_json;

use agent_model::clean_data::CleanData;
use agent_model::load_data::RawData;
use agent_model::load_data::Stats;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use clap::App;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::process;

#[derive(Debug)]
struct Args {
    input_fname: String,
    output_fname: String,
}

struct Io {
    output: BufWriter<File>,
    input: BufReader<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    process_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Processes hospital admissions csv data into json")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(input_arg())
        .arg(output_arg())
        .get_matches();

    let input = get_input(&matches)?;
    let output = get_output(&matches)?;
    let input_fname = get_input_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            input_fname,
            output_fname,
        },
        Io { input, output },
    ))
}

fn process_data(args: Args, io: Io) {
    println!("args: {:#?}", args);

    let (data, stats): (RawData, Stats) = RawData::from_lines(io.input).unwrap();
    println!("{:#?}", stats);

    let clean_data: CleanData = CleanData::clean_data(&data);
    println!("simulation steps: {}", clean_data.steps().len());

    serde_json::to_writer(io.output, &clean_data)
        .map_err(|e| ("could not save results", e))
        .unwrap();
}
