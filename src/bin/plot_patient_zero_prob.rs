extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::replay::SimulationOutput;
use agent_model::stats::reduce_step_data;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    input_fname: String,
    processed_fname: String,
    output_fname: String,
}

struct Io {
    input: BufReader<File>,
    processed: BufReader<File>,
    output: BufWriter<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plots number of simulations with non-zero count of infected people")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(input_arg())
        .arg(output_arg())
        .arg(processed_arg())
        .get_matches();

    let input = get_input(&matches)?;
    let processed = get_processed(&matches)?;
    let output = get_output(&matches)?;
    let input_fname = get_input_fname(&matches);
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            input_fname,
            processed_fname,
            output_fname,
        },
        Io {
            input,
            processed,
            output,
        },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);
    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    let procref = &processed;
    let fname = &args.input_fname;
    let mut input = zip::ZipArchive::new(io.input).unwrap();
    let mut infection_plots = (0..input.len())
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(move |index| {
            let mut input =
                zip::ZipArchive::new(BufReader::new(File::open(fname).unwrap())).unwrap();
            let input = std::io::BufReader::new(input.by_index(index).unwrap());
            let propagation: SimulationOutput = serde_json::from_reader(input).unwrap();
            let mut iterator = propagation.changes.iter().enumerate();
            let mut patient_zero = None;
            let mut start_step = None;
            while patient_zero.is_none() {
                let (s_step, changes) = iterator.next().unwrap();
                if let Some(p_zero) = changes.get(0) {
                    patient_zero = Some(*p_zero);
                    start_step = Some(s_step);
                }
            }
            let patient_zero = patient_zero.unwrap();
            let start_step = start_step.unwrap();
            let end_step = iterator
                .find(|(end_step, changes)| changes.contains(&patient_zero))
                .map(|(e, _)| e)
                .unwrap_or(propagation.changes.len());
            let mut endemic = false;
            reduce_step_data(&processed, &propagation, |hospitals, before, after| {
                endemic = after.iter().any(|v| *v)
            });
            (endemic, end_step - start_step)
        })
        .collect::<Vec<_>>();
    infection_plots.sort_by_key(|(e, t)| *t);
    for (endemic, time) in infection_plots {
        writeln!(io.output, "{} {}", time, if endemic {1} else {0}).unwrap();
    }
}
