extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::clean_data::HospitalOrd;
use agent_model::clean_data::PatientOrd;
use agent_model::replay::SimulationOutput;
use agent_model::stats::f64_cmp;
use agent_model::stats::hospitals_stats;
use agent_model::stats::sorted_to_stats;
use agent_model::stats::HospitalStats;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use itertools::Itertools;
use std::cmp::Ordering;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    processed_fname: String,
    output_fname: String,
}

struct Io {
    processed: BufReader<File>,
    output: BufWriter<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plot hospital size statistics based on processed data")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(output_arg())
        .arg(processed_arg())
        .get_matches();

    let processed = get_processed(&matches)?;
    let output = get_output(&matches)?;
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            processed_fname,
            output_fname,
        },
        Io { processed, output },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);

    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    let hospitals_stats = hospitals_stats(&processed);
    for (no, hospital) in hospitals_stats.iter().enumerate() {
        writeln!(
            io.output,
            "{} {:.3} {:.3} {:.3} {:.3} {:.3} {} {:.3}",
            no,
            hospital.min,
            hospital.perc25,
            hospital.median,
            hospital.perc75,
            hospital.max,
            hospital.ordinal,
            hospital.mean,
        )
        .unwrap();
    }
}
