extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::replay::SimulationOutput;
use agent_model::stats::reduce_step_data;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use agent_model::utils::FnvMap;
use clap::App;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    input_fname: String,
    processed_fname: String,
    output_fname: String,
}

struct Io {
    input: BufReader<File>,
    processed: BufReader<File>,
    output: BufWriter<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plots number of simulations with non-zero count of infected people")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(input_arg())
        .arg(output_arg())
        .arg(processed_arg())
        .get_matches();

    let input = get_input(&matches)?;
    let processed = get_processed(&matches)?;
    let output = get_output(&matches)?;
    let input_fname = get_input_fname(&matches);
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            input_fname,
            processed_fname,
            output_fname,
        },
        Io {
            input,
            processed,
            output,
        },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);
    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    let mut input = zip::ZipArchive::new(io.input).unwrap();
    let mut results = FnvMap::default();

    for index in 0..input.len() {
        let file = input.by_index(index).unwrap();
        let name = file.name().to_string();

        let input = std::io::BufReader::new(file);
        let propagation: SimulationOutput = serde_json::from_reader(input).unwrap();
        let mut replay = propagation.replay();
        let steps = propagation.changes.len() - 207;
        for _ in 0..steps {
            replay.next();
        }
        let mut at_hospital = vec![false; processed.patients().len()];
        let status = replay.status();
        processed.steps()[steps - 1]
            .iter()
            .flat_map(|h| h.into_iter())
            .for_each(|p| at_hospital[*p] = true);
        let mut n1 = 0;
        let mut i1 = 0;
        let mut i2 = 0;
        for i in 0..processed.patients().len() {
            n1 += at_hospital[i] as u32;
            i1 += (at_hospital[i] & status[i]) as u32;
            i2 += (!at_hospital[i] & status[i]) as u32;
        }
        let n2 = processed.patients().len() as u32 - n1;

        let params: Vec<i32> = serde_json::from_str(&name).unwrap();
        let beta_inv = params[0];
        let gamma_inv = params[1];
        results
            .entry((beta_inv, gamma_inv))
            .or_insert(Vec::new())
            .push((
                i1 as f32 / n1 as f32,
                i2 as f32 / n2 as f32,
                n1 as f32 / n2 as f32,
            ));
    }

    for ((beta_inv, gamma_inv), result_vec) in results {
        for (i1, i2, real_q) in result_vec {
            let gamma = 1.0 / gamma_inv as f32;
            let beta = 1.0 / beta_inv as f32;
            let common = beta * (i1 - 1.0) + gamma;
            let phi = -i1 * common / (i1 - i2);
            let q = -i2 * gamma / (i1 * common);
            writeln!(
                io.output,
                "{} {} {} {} {} {} {}",
                beta_inv, gamma_inv, i1, i2, phi, q, real_q
            )
            .unwrap();
        }
    }
}
