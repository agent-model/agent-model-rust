extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::replay::SimulationOutput;
use agent_model::stats::reduce_step_data;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    input_fname: String,
    processed_fname: String,
    output_fname: String,
}

struct Io {
    input: BufReader<File>,
    processed: BufReader<File>,
    output: BufWriter<File>,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plots number of infected people during simulations")
        .author("Paweł Włodarczyk-Pruszyński pwlodarczyk92@gmail.com")
        .version("0.1")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(input_arg())
        .arg(output_arg())
        .arg(processed_arg())
        .get_matches();

    let input = get_input(&matches)?;
    let processed = get_processed(&matches)?;
    let output = get_output(&matches)?;
    let input_fname = get_input_fname(&matches);
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);

    Ok((
        Args {
            input_fname,
            processed_fname,
            output_fname,
        },
        Io {
            input,
            processed,
            output,
        },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);
    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    let mut infection_plots = Vec::new();

    let mut input = zip::ZipArchive::new(io.input).unwrap();
    for index in 0..input.len() {
        let input = std::io::BufReader::new(input.by_index(index).unwrap());
        let propagation: SimulationOutput = serde_json::from_reader(input).unwrap();
        let is_infected = reduce_step_data(&processed, &propagation, |hospitals, before, after| {
            after.iter().any(|v| *v)
        });
        infection_plots.push(is_infected);
    }

    let mut entries = Vec::new();
    for step in 0..processed.steps().len() {
        entries.push(
            infection_plots
                .iter()
                .map(|p| p[step])
                .filter(|i| *i)
                .count(),
        );
    }
    for (step, entry) in entries.into_iter().enumerate() {
        let entry = entry;
        writeln!(io.output, "{} {}", step, entry).unwrap();
    }
}
