extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde_json;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::clean_data::HospitalOrd;
use agent_model::clean_data::PatientOrd;
use agent_model::replay::SimulationOutput;
use agent_model::utils::clap::get_hospital_ords;
use agent_model::utils::clap::get_input;
use agent_model::utils::clap::get_input_fname;
use agent_model::utils::clap::get_output;
use agent_model::utils::clap::get_output_fname;
use agent_model::utils::clap::get_processed;
use agent_model::utils::clap::get_processed_fname;
use agent_model::utils::clap::hospital_ords_arg;
use agent_model::utils::clap::input_arg;
use agent_model::utils::clap::output_arg;
use agent_model::utils::clap::processed_arg;
use clap::App;
use itertools::Itertools;
use std::cmp::Ordering;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::process;

#[derive(Debug)]
struct Args {
    processed_fname: String,
    output_fname: String,
    hospital_ords: Vec<HospitalOrd>,
}

struct Io {
    processed: BufReader<File>,
}

struct HospitalStats {
    mean: f64,
    median: f64,
    perc75: f64,
    perc25: f64,
    max: f64,
    min: f64,
}

fn main() {
    let (args, io) = match process_args() {
        Ok((args, io)) => (args, io),
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };
    plot_data(args, io);
}

fn process_args() -> Result<(Args, Io), String> {
    let matches = App::new("Plot hospital sizes based on processed data")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .arg(output_arg())
        .arg(processed_arg())
        .arg(hospital_ords_arg())
        .get_matches();

    let processed = get_processed(&matches)?;
    let processed_fname = get_processed_fname(&matches);
    let output_fname = get_output_fname(&matches);
    let hospital_ords = get_hospital_ords(&matches)?;

    Ok((
        Args {
            processed_fname,
            output_fname,
            hospital_ords,
        },
        Io { processed },
    ))
}

fn plot_data(args: Args, mut io: Io) {
    println!("args: {:#?}", args);

    let processed: CleanData = serde_json::from_reader(io.processed).unwrap();
    for ord in args.hospital_ords {
        let file = File::create(format!("{}-{}.txt", args.output_fname, ord)).unwrap();
        let mut file = BufWriter::new(file);
        let mut sizes = processed.steps().iter().map(|step| step[ord].len());
        for (step, size) in sizes.enumerate() {
            writeln!(file, "{} {}", step, size).unwrap();
        }
    }
}
