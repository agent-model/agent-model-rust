extern crate agent_model;
extern crate chrono;
extern crate itertools;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate flate2;
extern crate serde_json;
extern crate tar;
extern crate time;
extern crate zip;

use agent_model::clean_data::CleanData;
use agent_model::clean_data::HospitalOrd;
use agent_model::clean_data::Hospitals;
use agent_model::clean_data::PatientOrd;
use agent_model::replay::SimulationOutput;
use agent_model::simulation::global_infection_state;
use agent_model::simulation::Simulation;
use agent_model::simulation::Status;
use clap::App;
use rand::Isaac64Rng;
use rand::Rng;
use rand::SeedableRng;
use std::fs::read_to_string;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SimParams {
    beta_inv: f32,
    gamma_inv: f32,
    steps: usize,
    patients: usize,
    ratio: f32,
    seed: u64,
    output: String,
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let stdin = std::io::stdin();
    let params: Vec<SimParams> = serde_json::from_reader(BufReader::new(stdin.lock())).unwrap();
    let out = BufWriter::new(File::create(&args[1]).unwrap());
    let mut output = zip::ZipWriter::new(out);
    let default_opts = zip::write::FileOptions::default();

    for param in params {
        let result = run_simulation(&param);
        println!("simulating {:#?}", param);
        output.start_file(param.output, default_opts).unwrap();
        serde_json::to_writer(&mut output, &result).unwrap();
    }
}

fn run_simulation(params: &SimParams) -> Vec<f32> {
    let beta = 1.0 / (params.beta_inv * 2.0);
    let gamma = 1.0 / (params.gamma_inv * 2.0);
    let mut random = Isaac64Rng::from_seed(&[
        12555,
        0,
        (4842728u64).wrapping_add(params.seed.wrapping_mul(3155u64)),
    ]);

    let initial_state = global_infection_state(params.patients, params.ratio, &mut random);
    let mut simulation = Simulation::new(initial_state, gamma, beta, random);
    let mut result = vec![simulation.infection_table().global_infection_rate()];
    let step_vec = (0..params.patients).collect::<Vec<_>>();
    for step in 0..params.steps {
        simulation.step(&[&step_vec]);
        result.push(simulation.infection_table().global_infection_rate());
    }
    result
}
