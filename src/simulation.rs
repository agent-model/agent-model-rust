use crate::clean_data::PatientOrd;
use crate::utils::FnvSet;
use rand::Isaac64Rng;
use rand::Rng;

pub struct Simulation {
    infected_table: Vec<bool>,
    recovery_coeff: f32,
    transmission_coeff: f32,
    random: Isaac64Rng,
}

impl Simulation {
    pub fn new(
        infected_table: Vec<bool>,
        recovery_coeff: f32,
        transmission_coeff: f32,
        random: Isaac64Rng,
    ) -> Simulation {
        Simulation {
            infected_table,
            recovery_coeff,
            transmission_coeff,
            random,
        }
    }

    pub fn force_changes(&mut self, changes: Vec<PatientOrd>) -> Vec<PatientOrd> {
        for patient in changes.iter().cloned() {
            self.infected_table[patient] = !self.infected_table[patient]
        }
        changes
    }

    pub fn force_state(&mut self, state: Vec<bool>) -> Vec<PatientOrd> {
        assert_eq!(state.len(), self.infected_table.len());
        let mut changes = Vec::new();
        for i in 0..state.len() {
            if self.infected_table[i] != state[i] {
                changes.push(i);
            }
        }
        self.infected_table = state;
        changes
    }

    pub fn step<T: AsRef<[PatientOrd]>>(&mut self, hospitals: &[T]) -> Vec<PatientOrd> {
        let mut changes = Vec::new();
        let mut all_patients = FnvSet::default();
        for hospital in hospitals {
            let hospital = hospital.as_ref();
            if hospital.len() != 0 {
                all_patients.extend(hospital.iter());
                self.simulate_hospital(hospital, &mut changes);
            }
        }

        let mut recovered = self.simulate_home(all_patients);
        changes.append(&mut recovered);
        changes
    }

    fn simulate_hospital(&mut self, patients: &[PatientOrd], changes: &mut Vec<PatientOrd>) {
        let patients_count = patients.len();
        let infected_count = patients.iter().filter(|p| self.infected_table[**p]).count();

        let transmission_probability =
            self.transmission_coeff * (infected_count as f32) / (patients_count as f32);
        let recovery_probability = self.recovery_coeff;
        for patient in patients {
            if !self.infected_table[*patient] {
                if transmission_probability > 0.0
                    && self.random.next_f32() < transmission_probability
                {
                    self.infected_table[*patient] = true;
                    changes.push(*patient);
                }
            } else {
                if self.random.next_f32() < recovery_probability {
                    self.infected_table[*patient] = false;
                    changes.push(*patient);
                }
            }
        }
    }

    fn simulate_home(&mut self, all_patients: FnvSet<PatientOrd>) -> Vec<PatientOrd> {
        let recovery_probability = self.recovery_coeff;

        let mut recovered = Vec::new();
        for (person, &infected) in self.infected_table.iter().enumerate() {
            if infected
                && !all_patients.contains(&person)
                && self.random.next_f32() < recovery_probability
            {
                recovered.push(person);
            }
        }
        for person in recovered.iter() {
            self.infected_table[*person] = false;
        }
        recovered
    }

    pub fn infection_table(&self) -> &[bool] {
        &self.infected_table
    }

    pub fn random(&mut self) -> &mut Isaac64Rng {
        &mut self.random
    }
}

pub trait Status {
    fn infection_count(&self, hospital: &[PatientOrd]) -> usize;
    fn hospitals_infection_count(&self, hospitals: &[Vec<PatientOrd>]) -> usize;
    fn global_infection_count(&self) -> usize;
    fn home_infection_count(&self, hospitals: &[Vec<PatientOrd>]) -> usize;
    fn infection_rate(&self, hospital: &[PatientOrd]) -> Option<f32>;
    fn hospitals_infection_rate(&self, hospitals: &[Vec<PatientOrd>]) -> f32;
    fn global_infection_rate(&self) -> f32;
    fn home_infection_rate(&self, hospitals: &[Vec<PatientOrd>]) -> f32;
}

impl Status for [bool] {
    fn infection_count(&self, hospital: &[PatientOrd]) -> usize {
        hospital.iter().filter(|p| self[**p]).count()
    }

    fn hospitals_infection_count(&self, hospitals: &[Vec<PatientOrd>]) -> usize {
        hospitals
            .iter()
            .flat_map(|h| h.iter())
            .filter(|p| self[**p])
            .count()
    }

    fn global_infection_count(&self) -> usize {
        self.iter().filter(|p| **p).count()
    }

    fn home_infection_count(&self, hospitals: &[Vec<PatientOrd>]) -> usize {
        self.global_infection_count() - self.hospitals_infection_count(hospitals)
    }

    fn infection_rate(&self, hospital: &[PatientOrd]) -> Option<f32> {
        if hospital.len() == 0 {
            None
        } else {
            Some(self.infection_count(hospital) as f32 / hospital.len() as f32)
        }
    }

    fn hospitals_infection_rate(&self, hospitals: &[Vec<PatientOrd>]) -> f32 {
        let all: usize = hospitals.iter().map(|h| h.len()).sum();
        self.hospitals_infection_count(hospitals) as f32 / all as f32
    }

    fn global_infection_rate(&self) -> f32 {
        self.global_infection_count() as f32 / self.len() as f32
    }

    fn home_infection_rate(&self, hospitals: &[Vec<PatientOrd>]) -> f32 {
        let patients_count: usize = hospitals.iter().map(|h| h.len()).sum();
        let all_count: usize = self.len();
        self.home_infection_count(hospitals) as f32 / (all_count - patients_count) as f32
    }
}

pub fn global_infection_state(
    patients_count: usize,
    infected_fraction: f32,
    rand: &mut Isaac64Rng,
) -> Vec<bool> {
    let infected_count = (patients_count as f32 * infected_fraction).round() as usize;
    let patients: Vec<PatientOrd> = (0..patients_count).map(|i| i as PatientOrd).collect();
    let mut shuffled = patients.clone();
    rand.shuffle(shuffled.as_mut());
    let infected: FnvSet<PatientOrd> = shuffled.into_iter().take(infected_count).collect();

    patients
        .into_iter()
        .map(|p| infected.contains(&p))
        .collect()
}
