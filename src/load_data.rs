use crate::errors::BadDate;
use crate::errors::BadInteger;
use crate::errors::LineError;
use crate::errors::LoadError;
use crate::errors::VisitError;
use crate::utils::FnvMap;
use crate::utils::FnvSet;
use chrono::NaiveDate;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter;

#[derive(Default)]
pub struct RawData {
    patients: FnvMap<PatientId, RawPatient>,
    hospitals: FnvSet<HospitalId>,
}

pub struct RawPatient {
    id: PatientId,
    visits: Vec<RawVisit>,
}

#[derive(Clone, Copy, Debug)]
pub struct RawVisit {
    admission: NaiveDate,
    dismiss: NaiveDate,
    hospital: HospitalId,
    line_num: usize,
}

#[derive(Default, Debug)]
pub struct Stats {
    pub removed_duplicates: usize,
    pub hospitals: usize,
    pub unique_lines: usize,
    pub removed_days: usize,
    pub remaining_days: usize,
    pub removed_patients: usize,
    pub remaining_patients: usize,
}

pub type PatientId = i32;
pub type HospitalId = i32;

impl RawData {
    pub fn from_lines(reader: BufReader<File>) -> Result<(RawData, Stats), LoadError> {
        let mut result = RawData::default();
        let mut stats = Stats::default();
        let mut lines = reader.lines();
        let mut entries: Vec<(usize, String)> = Vec::new();

        let header: String = lines.next().ok_or(LoadError::MissingHeader)??;
        result.read_header(&header)?;

        for (line_num, line) in lines.enumerate() {
            entries.push((line_num, line?));
        }
        let all_lines = entries.len();

        entries.sort_by(|(_i1, line1), (_i2, line2)| line1.cmp(line2));
        entries.dedup_by(|(_i1, line1), (_i2, line2)| line1 == line2);
        stats.unique_lines = entries.len();
        stats.removed_duplicates = all_lines - stats.unique_lines;

        for (num, line) in entries {
            let (id, visit) = result.read_enumerated_line(&line, num)?;
            result.hospitals.insert(visit.hospital());
            result
                .patients
                .entry(id)
                .or_insert_with(|| RawPatient::new(id))
                .visits
                .push(visit);
        }

        for (_, patient) in result.patients.iter_mut() {
            for removed in patient.cleanup_overlaps()? {
                stats.removed_days += removed.days();
            }
        }

        stats.remaining_days = result
            .patients
            .values()
            .flat_map(|p| p.visits().iter())
            .map(|v| v.days())
            .sum();

        if stats.remaining_days == 0 {
            return Err(LoadError::EmptyDataset(stats));
        }

        let all_patients = result.patients.len();
        result.patients.retain(|_k, p| p.visits().len() > 0);
        stats.remaining_patients = result.patients().len();
        stats.removed_patients = all_patients - stats.remaining_patients;
        stats.hospitals = result.hospitals.len();

        Ok((result, stats))
    }

    fn read_header(&self, line: &str) -> Result<(), LoadError> {
        if line == "Patient id	sex	admission	dismiss	hospital	region" {
            Ok(())
        } else {
            Err(LoadError::BadHeader(line.to_string()))
        }
    }

    fn read_enumerated_line(
        &mut self,
        line: &str,
        line_num: usize,
    ) -> Result<(PatientId, RawVisit), LoadError> {
        self.read_line(line, line_num).map_err(|error| {
            LoadError::BadLine(LineError {
                error,
                line: line.to_string(),
                line_num,
            })
        })
    }

    fn read_line(
        &mut self,
        line: &str,
        line_num: usize,
    ) -> Result<(PatientId, RawVisit), VisitError> {
        let parts: Vec<&str> = line.split_whitespace().collect();
        if let &[id_str, _sex, admission_str, dismiss_str, hospital_str, _region] = parts.as_slice()
        {
            let id: PatientId = str::parse(id_str).map_err(|e| (e, BadInteger::PatientId))?;
            let hospital: HospitalId =
                str::parse(hospital_str).map_err(|e| (e, BadInteger::HospitalId))?;
            let admission = NaiveDate::parse_from_str(admission_str, "%Y-%m-%d")
                .map_err(|e| (e, BadDate::Admission))?;
            let dismiss = NaiveDate::parse_from_str(dismiss_str, "%Y-%m-%d")
                .map_err(|e| (e, BadDate::Dismiss))?;
            let visit = RawVisit::new(admission, dismiss, hospital, line_num)?;
            Ok((id, visit))
        } else {
            Err(VisitError::BadEntryFormat)
        }
    }

    pub fn patients(&self) -> &FnvMap<PatientId, RawPatient> {
        &self.patients
    }

    pub fn hospitals(&self) -> &FnvSet<HospitalId> {
        &self.hospitals
    }
}

impl RawPatient {
    fn new(id: PatientId) -> RawPatient {
        RawPatient {
            id,
            visits: Vec::new(),
        }
    }

    //returns visits removed from the dataset
    fn cleanup_overlaps(&mut self) -> Result<Vec<RawVisit>, LoadError> {
        self.visits.sort_by_key(|p| p.timerange());
        self.validate_overlaps()?;
        return Ok(self.remove_ambiguous_overlaps());
    }

    fn validate_overlaps(&self) -> Result<(), LoadError> {
        let mut windows = self.visits.windows(2);
        while let Some([before, after]) = windows.next() {
            if before.admission > after.admission {
                panic!("this should've been sorted!");
            }
            if before.dismiss > after.admission {
                return Err(LoadError::BadOverlap(self.id, *before, *after));
            }
        }
        return Ok(());
    }

    fn remove_ambiguous_overlaps(&mut self) -> Vec<RawVisit> {
        let mut single_day_visit_overlap_days = Vec::new();
        let mut three_visits_same_day_days = Vec::new();
        {
            let mut visits_pairs = self.visits.windows(2);
            while let Some([former, latter]) = visits_pairs.next() {
                if former.admission == latter.admission && former.dismiss == latter.dismiss {
                    single_day_visit_overlap_days.push(former.admission);
                }
            }
            let mut visits_triplets = self.visits.windows(3);
            while let Some([former, current, latter]) = visits_triplets.next() {
                if former.dismiss == current.admission
                    && current.admission == current.dismiss
                    && current.dismiss == latter.admission
                {
                    three_visits_same_day_days.push(current.admission);
                }
            }
        }

        let undecidable_days: FnvSet<NaiveDate> = single_day_visit_overlap_days
            .into_iter()
            .chain(three_visits_same_day_days.into_iter())
            .collect();

        let all_visits = std::mem::replace(&mut self.visits, Vec::new());
        let (removed, kept) = all_visits
            .into_iter()
            .partition(|v| v.admission == v.dismiss && undecidable_days.contains(&v.admission));
        self.visits = kept;
        removed
    }

    pub fn id(&self) -> PatientId {
        self.id
    }

    pub fn visits(&self) -> &[RawVisit] {
        &self.visits
    }
}

impl RawVisit {
    pub fn new(
        admission: NaiveDate,
        dismiss: NaiveDate,
        hospital: HospitalId,
        line_num: usize,
    ) -> Result<RawVisit, VisitError> {
        if dismiss < admission {
            return Err((admission, dismiss).into());
        }
        Ok(RawVisit {
            admission,
            dismiss,
            hospital,
            line_num,
        })
    }
    pub fn admission(&self) -> NaiveDate {
        self.admission
    }
    pub fn dismiss(&self) -> NaiveDate {
        self.dismiss
    }
    pub fn hospital(&self) -> HospitalId {
        self.hospital
    }
    pub fn timerange(&self) -> (NaiveDate, NaiveDate) {
        (self.admission, self.dismiss)
    }
    pub fn timerange_iter(&self) -> impl Iterator<Item = NaiveDate> {
        iter::once(self.admission).chain(iter::once(self.dismiss))
    }
    pub fn days(&self) -> usize {
        ((self.dismiss - self.admission).num_days() + 1) as usize
    }
}
