use clap::Arg;
use clap::ArgMatches;
use std::fs::File;
use std::io::BufReader;
use std::io::BufWriter;
use std::string::ToString;

pub trait ClapArg<'a, 'b> {
    fn new(name: &'a str, short: &'a str, long: &'a str) -> Self;
    fn with_long(name: &'a str, long: &'a str) -> Self;
    fn required_value(self, name: &'a str) -> Self;
    fn optional_value(self, name: &'a str) -> Self;
}

impl<'a, 'b> ClapArg<'a, 'b> for clap::Arg<'a, 'b> {
    fn new(name: &'a str, short: &'a str, long: &'a str) -> Self {
        clap::Arg::with_name(name).short(short).long(long)
    }

    fn with_long(name: &'a str, long: &'a str) -> Self {
        clap::Arg::with_name(name).long(long)
    }

    fn required_value(self, name: &'a str) -> Self {
        self.takes_value(true).value_name(name).required(true)
    }

    fn optional_value(self, name: &'a str) -> Self {
        self.takes_value(true).value_name(name)
    }
}

pub const VERBOSE: &'static str = "verbose";
pub const INPUT: &'static str = "input";
pub const PROCESSED: &'static str = "processed";
pub const OUTPUT: &'static str = "output";
pub const SEED: &'static str = "seed";
pub const BETA: &'static str = "beta";
pub const GAMMA: &'static str = "gamma";
pub const RATIO: &'static str = "ratio";
pub const HOSPITALS: &'static str = "hospitals";
pub const PATIENTS: &'static str = "patients";
pub const HOSPITAL: &'static str = "hospital";
pub const STEP: &'static str = "step";

pub fn verbose_arg() -> Arg<'static, 'static> {
    Arg::new(VERBOSE, "v", "verbose")
        .multiple(true)
        .help("Specifies the level of verbosity")
}

pub fn input_arg() -> Arg<'static, 'static> {
    Arg::new(INPUT, "i", "input")
        .required_value("FILE-PATH")
        .help("Specifies input file path")
}

pub fn processed_arg() -> Arg<'static, 'static> {
    Arg::new(PROCESSED, "p", "processed")
        .required_value("FILE-PATH")
        .help("Specifies file path with processed data json")
}

pub fn output_arg() -> Arg<'static, 'static> {
    Arg::new(OUTPUT, "o", "output")
        .required_value("FILE-PATH")
        .help("Specifies output file path")
}

pub fn seed_arg() -> Arg<'static, 'static> {
    Arg::new(SEED, "s", "seed")
        .required_value("POSITIVE-INTEGER")
        .help("Specifies seed")
}

pub fn beta_arg() -> Arg<'static, 'static> {
    Arg::new(BETA, "b", "beta-inv")
        .required_value("STEPS")
        .help("Specifies inverse of the beta parameter (unit=STEPS)")
}

pub fn gamma_arg() -> Arg<'static, 'static> {
    Arg::new(GAMMA, "g", "gamma-inv")
        .required_value("STEPS")
        .help("Specifies inverse of the gamma parameter (unit=STEPS)")
}

pub fn ratio_arg() -> Arg<'static, 'static> {
    Arg::new(RATIO, "r", "ratio")
        .required_value("RATIO")
        .help("Specifies initial ratio of infected agents to all agents")
}

pub fn hospital_ords_arg() -> Arg<'static, 'static> {
    Arg::new(HOSPITALS, "h", "hospitals")
        .required_value("ORDINALS")
        .multiple(true)
        .help("specifies hospitals using ordinals")
}

pub fn patient_ords_arg() -> Arg<'static, 'static> {
    Arg::with_long(PATIENTS, "patients")
        .required_value("ORDINALS")
        .help("specifies patients using ordinals")
}

pub fn step_arg() -> Arg<'static, 'static> {
    Arg::with_long(STEP, "step")
        .required_value("STEP")
        .help("specifies initial step")
}

pub fn get_verbose(matches: &ArgMatches<'static>) -> u64 {
    matches.occurrences_of(VERBOSE)
}

pub fn get_input(matches: &ArgMatches<'static>) -> Result<BufReader<File>, String> {
    matches
        .value_of(INPUT)
        .map(|a| File::open(a))
        .unwrap()
        .map_err(|e| format!("couldn't open input file: {}", e))
        .map(|f| BufReader::new(f))
}

pub fn get_input_fname(matches: &ArgMatches<'static>) -> String {
    matches.value_of(INPUT).unwrap().to_string()
}

pub fn get_processed(matches: &ArgMatches<'static>) -> Result<BufReader<File>, String> {
    matches
        .value_of(PROCESSED)
        .map(|a| File::open(a))
        .unwrap()
        .map_err(|e| format!("couldn't open processed data file: {}", e))
        .map(|f| BufReader::new(f))
}

pub fn get_processed_fname(matches: &ArgMatches<'static>) -> String {
    matches.value_of(PROCESSED).unwrap().to_string()
}

pub fn get_output(matches: &ArgMatches<'static>) -> Result<BufWriter<File>, String> {
    matches
        .value_of(OUTPUT)
        .map(|a| File::create(a))
        .unwrap()
        .map_err(|e| format!("couldn't create output file: {}", e))
        .map(|f| BufWriter::new(f))
}

pub fn get_output_fname(matches: &ArgMatches<'static>) -> String {
    matches.value_of(OUTPUT).unwrap().to_string()
}

pub fn get_seed(matches: &ArgMatches<'static>) -> Result<u32, String> {
    matches
        .value_of(SEED)
        .map(|a| str::parse(&a))
        .unwrap()
        .map_err(|e| format!("invalid seed: {}", e))
}

pub fn get_beta(matches: &ArgMatches<'static>) -> Result<f32, String> {
    matches
        .value_of(BETA)
        .map(|a| str::parse::<f32>(&a))
        .unwrap()
        .map_err(|e| format!("invalid beta: {}", e))
        .map(|inv_beta| 1.0 / inv_beta)
        .and_then(|b| {
            if 0.0 <= b {
                Ok(b)
            } else {
                Err(format!("beta: {:.3} is lower than zero", b))
            }
        })
}

pub fn get_gamma(matches: &ArgMatches<'static>) -> Result<f32, String> {
    matches
        .value_of(GAMMA)
        .map(|a| str::parse::<f32>(&a))
        .unwrap()
        .map_err(|e| format!("invalid gamma: {}", e))
        .map(|inv_gamma| 1.0 / inv_gamma)
        .and_then(|g| {
            if 0.0 <= g {
                Ok(g)
            } else {
                Err(format!("gamma: {:.3} is lower than zero", g))
            }
        })
}

pub fn get_ratio(matches: &ArgMatches<'static>) -> Result<f32, String> {
    matches
        .value_of(RATIO)
        .map(|a| str::parse(&a))
        .unwrap()
        .map_err(|e| format!("invalid ratio: {}", e))
        .and_then(|r| {
            if 0.0 <= r && r <= 1.0 {
                Ok(r)
            } else {
                Err(format!("ratio: {:.3} is not in range [0.0, 1.0]", r))
            }
        })
}

pub fn get_hospital_ords(matches: &ArgMatches<'static>) -> Result<Vec<usize>, String> {
    matches
        .values_of(HOSPITALS)
        .map(|args| {
            args.into_iter()
                .map(|arg| str::parse(&arg))
                .collect::<Result<Vec<_>, _>>()
        })
        .unwrap()
        .map_err(|e| format!("invalid hospital ordinal: {}", e))
}

pub fn get_patient_ords(matches: &ArgMatches<'static>) -> Result<Vec<usize>, String> {
    matches
        .values_of(PATIENTS)
        .map(|args| {
            args.into_iter()
                .map(|arg| str::parse(&arg))
                .collect::<Result<Vec<_>, _>>()
        })
        .unwrap()
        .map_err(|e| format!("invalid patient ordinal: {}", e))
}

pub fn get_hospital_ord(matches: &ArgMatches<'static>) -> Result<usize, String> {
    matches
        .value_of(HOSPITAL)
        .map(|arg| str::parse(arg))
        .unwrap()
        .map_err(|e| format!("invalid hospital ordinal: {}", e))
}

pub fn get_step(matches: &ArgMatches<'static>) -> Result<usize, String> {
    matches
        .value_of(STEP)
        .map(|arg| str::parse(arg))
        .unwrap()
        .map_err(|e| format!("invalid step: {}", e))
}
