use fnv::FnvBuildHasher;
use indexmap::IndexMap;
use indexmap::IndexSet;

pub mod clap;

pub type FnvMap<K, V> = IndexMap<K, V, FnvBuildHasher>;
pub type FnvSet<K> = IndexSet<K, FnvBuildHasher>;

/*
merge changes

        last_changes.extend(changes);
        last_changes.sort();
        let mut new_changes = Vec::new();
        let mut seen = false;
        for i in 0..last_changes.len() - 1 {
            match (last_changes[i] == last_changes[i+1], seen) {
                (true, _) => seen = true,
                (false, true) => seen = false,
                (false, false) => new_changes.push(last_changes[i])
            }
        }
        if !seen {
            if let Some(last) = last_changes.last() {
                new_changes.push(*last);
            }
        }
        *last_changes = new_changes;

*/
