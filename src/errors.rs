use crate::load_data;
use crate::load_data::Stats;
use chrono;
use std;

#[derive(Debug)]
pub enum LoadError {
    EmptyDataset(Stats),
    IoError(std::io::Error),
    MissingHeader,
    BadHeader(String),
    BadLine(LineError),
    BadOverlap(
        load_data::PatientId,
        load_data::RawVisit,
        load_data::RawVisit,
    ),
}

#[derive(Debug)]
pub struct LineError {
    pub error: VisitError,
    pub line: String,
    pub line_num: usize,
}

#[derive(Debug)]
pub enum BadDate {
    Admission,
    Dismiss,
}

#[derive(Debug)]
pub enum BadInteger {
    PatientId,
    HospitalId,
}

#[derive(Debug)]
pub enum VisitError {
    BadPatientId(std::num::ParseIntError),
    BadHospitalId(std::num::ParseIntError),
    BadAdmission(chrono::ParseError),
    BadDismiss(chrono::ParseError),
    ReversedDates(chrono::NaiveDate, chrono::NaiveDate),
    BadEntryFormat,
}

impl From<std::io::Error> for LoadError {
    fn from(err: std::io::Error) -> Self {
        LoadError::IoError(err)
    }
}

impl From<LineError> for LoadError {
    fn from(err: LineError) -> Self {
        LoadError::BadLine(err)
    }
}

impl LineError {
    pub fn new(error: VisitError, line: String, line_num: usize) -> LineError {
        LineError {
            error,
            line,
            line_num,
        }
    }
}

impl From<(std::num::ParseIntError, BadInteger)> for VisitError {
    fn from((err, t): (std::num::ParseIntError, BadInteger)) -> Self {
        match t {
            BadInteger::PatientId => VisitError::BadPatientId(err),
            BadInteger::HospitalId => VisitError::BadHospitalId(err),
        }
    }
}

impl From<(chrono::ParseError, BadDate)> for VisitError {
    fn from((err, t): (chrono::ParseError, BadDate)) -> Self {
        match t {
            BadDate::Admission => VisitError::BadAdmission(err),
            BadDate::Dismiss => VisitError::BadDismiss(err),
        }
    }
}

impl From<(chrono::NaiveDate, chrono::NaiveDate)> for VisitError {
    fn from((admission, dismiss): (chrono::NaiveDate, chrono::NaiveDate)) -> Self {
        VisitError::ReversedDates(admission, dismiss)
    }
}
