pub enum Transition {
    StoS,
    StoI,
    ItoS,
    ItoI,
}

#[derive(Serialize, Deserialize)]
pub struct SimulationOutput {
    pub initial_state: Vec<bool>,
    pub changes: Vec<Vec<usize>>,
}

pub struct Replay<'a> {
    propagation: &'a SimulationOutput,
    current_state: Vec<bool>,
    step: usize,
}

impl SimulationOutput {
    pub fn replay(&self) -> Replay {
        Replay {
            propagation: self,
            current_state: self.initial_state.clone(),
            step: 0,
        }
    }
}

impl<'a> Replay<'a> {
    pub fn next(&mut self) {
        if let Some(changes) = self.propagation.changes.get(self.step) {
            changes
                .iter()
                .for_each(|patient| self.current_state[*patient] = !self.current_state[*patient]);
            self.step += 1;
        } else {
            panic!("all steps executed")
        }
    }
    pub fn status(&self) -> &[bool] {
        &self.current_state
    }
    pub fn get_next_step(&self) -> usize {
        self.step
    }
    pub fn get_last_step(&self) -> usize {
        assert!(self.step > 0);
        self.step - 1
    }
    pub fn is_last(&self) -> bool {
        self.step == self.propagation.changes.len()
    }
}
