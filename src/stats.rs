use crate::clean_data::CleanData;
use crate::clean_data::HospitalOrd;
use crate::replay::Replay;
use crate::replay::SimulationOutput;
use itertools::Itertools;
use std::cmp::Ordering;

pub struct HospitalStats {
    pub ordinal: HospitalOrd,
    pub mean: f64,
    pub median: f64,
    pub perc75: f64,
    pub perc25: f64,
    pub max: f64,
    pub min: f64,
}

pub fn hospitals_stats(processed: &CleanData) -> Vec<HospitalStats> {
    let mut hospital_data: Vec<(HospitalOrd, Vec<f64>)> = processed
        .hospitals()
        .iter()
        .enumerate()
        .map(|(ord, _h)| (ord, Vec::new()))
        .collect::<Vec<_>>();

    for (step_no, step) in processed.steps().iter().enumerate() {
        for (hospital_ord, hospital) in step.iter().enumerate() {
            hospital_data[hospital_ord].1.push(hospital.len() as f64);
        }
    }
    hospital_data
        .iter_mut()
        .for_each(|(_, h)| h.sort_by(f64_cmp));

    let mut hospitals_stats: Vec<HospitalStats> = hospital_data
        .iter()
        .map(|patient_counts| sorted_to_stats(patient_counts.0, &patient_counts.1))
        .collect();
    hospitals_stats.sort_by(|h1, h2| f64_cmp(&h1.median, &h2.median));
    hospitals_stats
}

pub fn sorted_to_stats(ordinal: HospitalOrd, sorted_values: &[f64]) -> HospitalStats {
    HospitalStats {
        ordinal,
        mean: sorted_values.iter().sum::<f64>() / (sorted_values.len() as f64),
        median: sorted_to_percentile(sorted_values, 50.0),
        perc75: sorted_to_percentile(sorted_values, 75.0),
        perc25: sorted_to_percentile(sorted_values, 25.0),
        max: sorted_values
            .iter()
            .cloned()
            .fold1(|a, b| a.max(b))
            .unwrap(),
        min: sorted_values
            .iter()
            .cloned()
            .fold1(|a, b| a.min(b))
            .unwrap(),
    }
}

pub fn sorted_to_percentile(sorted_values: &[f64], percentile: f64) -> f64 {
    if sorted_values.len() == 1 {
        return sorted_values[0];
    }
    let zero: f64 = 0.0;
    assert!(zero <= percentile);
    let hundred = 100f64;
    assert!(percentile <= hundred);
    if percentile == hundred {
        return sorted_values[sorted_values.len() - 1];
    }

    let length = (sorted_values.len() - 1) as f64;
    let rank = (percentile / hundred) * length;
    let lrank = rank.floor();
    let d = rank - lrank;
    let n = lrank as usize;
    let lo = sorted_values[n];
    let hi = sorted_values[n + 1];
    lo + (hi - lo) * d
}

pub fn f64_cmp(x: &f64, y: &f64) -> Ordering {
    if y.is_nan() || x.is_nan() {
        panic!()
    } else if x < y {
        Ordering::Less
    } else if x == y {
        Ordering::Equal
    } else {
        Ordering::Greater
    }
}

pub fn reduce_hospital_data<T, F: Fn(&[usize], &[bool], &[bool]) -> T>(
    processed: &CleanData,
    simulation: &SimulationOutput,
    f: F,
) -> Vec<Vec<T>> {
    let mut replay = simulation.replay();
    let mut last_status = replay.status().to_vec();
    let mut result = Vec::new();
    while !replay.is_last() {
        replay.next();
        let next_status = replay.status().to_vec();
        let step_result = processed.steps()[replay.get_last_step()]
            .iter()
            .map(|hospital| f(hospital, &last_status, &next_status))
            .collect::<Vec<_>>();
        result.push(step_result);
        last_status = next_status;
    }
    result
}

pub fn reduce_step_data<T, F: FnMut(&[Vec<usize>], &[bool], &[bool]) -> T>(
    processed: &CleanData,
    simulation: &SimulationOutput,
    mut f: F,
) -> Vec<T> {
    let mut replay = simulation.replay();
    let mut last_status = replay.status().to_vec();
    let mut result = Vec::new();
    while !replay.is_last() {
        replay.next();
        let next_status = replay.status().to_vec();
        let step_result = f(
            &processed.steps()[replay.get_last_step()],
            &last_status,
            &next_status,
        );
        result.push(step_result);
        last_status = next_status;
    }
    result
}
