use crate::load_data::{HospitalId, PatientId, RawData, RawPatient, RawVisit};
use crate::utils::FnvMap;
use chrono::{Duration, NaiveDate};
use itertools::Itertools;
use std::iter;

struct CleanVisit {
    admission: Iteration,
    dismiss: Iteration,
    hospital: HospitalId,
}

#[derive(Serialize, Deserialize)]
pub struct CleanData {
    hospitals: Vec<HospitalId>,
    patients: Vec<PatientId>,
    steps: Steps,
}

pub type Steps = Vec<Hospitals>;
pub type Hospitals = Vec<Patients>;
pub type Patients = Vec<PatientOrd>;
pub type PatientOrd = usize;
pub type HospitalOrd = usize;
pub type Iteration = usize;

impl CleanData {
    pub fn clean_data(data: &RawData) -> CleanData {
        let mut patients: Vec<PatientId> = data.patients().iter().map(|(i, _)| *i).collect();
        patients.sort();
        let patient_id_to_ord: FnvMap<PatientId, PatientOrd> =
            patients.iter().enumerate().map(|(i, p)| (*p, i)).collect();

        let mut hospitals: Vec<HospitalId> = data.hospitals().iter().map(|h| *h).collect();
        hospitals.sort();
        let hospital_id_to_ord: FnvMap<HospitalId, HospitalOrd> =
            hospitals.iter().enumerate().map(|(i, h)| (*h, i)).collect();

        let day_range: (NaiveDate, NaiveDate) = data
            .patients()
            .iter()
            .flat_map(|(_i, p)| p.visits().iter())
            .map(|visit| visit.timerange())
            .fold1(|range: (NaiveDate, NaiveDate), visit| {
                (range.0.min(visit.0), range.1.max(visit.1))
            })
            .expect("empty dataset!");

        let mut steps: Steps = Vec::new();
        let days_count = ((day_range.1 - day_range.0).num_days() + 1) as Iteration;
        for _step_num in 0..(days_count) * 2 {
            steps.push(vec![Vec::new(); data.hospitals().len()]);
        }

        for patient in patients.iter().map(|p| data.patients().get(p).unwrap()) {
            let patient_ord = *patient_id_to_ord.get(&patient.id()).unwrap();
            let visits = clean_visits(patient, day_range.0);
            for CleanVisit {
                admission,
                dismiss,
                hospital,
            } in visits.into_iter()
            {
                let hospital_ord = *hospital_id_to_ord.get(&hospital).unwrap();
                for iter_num in admission..=dismiss {
                    steps[iter_num as Iteration][hospital_ord].push(patient_ord)
                }
            }
        }

        CleanData {
            hospitals,
            patients,
            steps,
        }
    }

    pub fn hospitals(&self) -> &Vec<i32> {
        &self.hospitals
    }
    pub fn patients(&self) -> &Vec<i32> {
        &self.patients
    }
    pub fn steps(&self) -> &Vec<Hospitals> {
        &self.steps
    }
}

fn clean_visits(patient: &RawPatient, reference: NaiveDate) -> Vec<CleanVisit> {
    if patient.visits().len() == 0 {
        return Vec::new();
    }

    let first = patient.visits().first().unwrap();
    let dummy_start = RawVisit::new(
        first.admission() - Duration::days(1),
        first.admission() - Duration::days(1),
        -1,
        usize::max_value(),
    )
    .unwrap();

    let last = patient.visits().last().unwrap();
    let dummy_end = RawVisit::new(
        last.dismiss() + Duration::days(1),
        last.dismiss() + Duration::days(1),
        -1,
        usize::max_value(),
    )
    .unwrap();

    let visits_with_dummies: Vec<&RawVisit> = iter::once(&dummy_start)
        .chain(patient.visits().iter())
        .chain(iter::once(&dummy_end))
        .collect();
    let mut windows = visits_with_dummies.windows(3);
    let mut result = Vec::new();
    while let Some(&[former, current, latter]) = windows.next() {
        let admission = if former.dismiss() == current.admission() {
            ((current.admission() - reference).num_days() * 2 + 1) as Iteration
        } else {
            ((current.admission() - reference).num_days() * 2) as Iteration
        };
        let dismiss = if current.dismiss() == latter.admission() {
            ((current.dismiss() - reference).num_days() * 2) as Iteration
        } else {
            ((current.dismiss() - reference).num_days() * 2 + 1) as Iteration
        };
        result.push(CleanVisit {
            admission,
            dismiss,
            hospital: current.hospital(),
        });
    }
    result
}
