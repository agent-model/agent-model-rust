# Agent model implementation
This repository contains implementation of agent model of hospital network for the purpose of bachelor thesis.
Simulation logic is placed in `src/simulation.rs` source file.
- `mkdir data`
- place `*.csv` file containing hospital visits data in `./data/` directory
- `./do-everything.sh`
