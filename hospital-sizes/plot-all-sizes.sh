#!/usr/bin/env bash
ARGS=`cut -d' ' -f7 < ./plots/hospitals-stats.txt | paste -sd " " -`
./plot-hospitals-sizes.sh $ARGS
