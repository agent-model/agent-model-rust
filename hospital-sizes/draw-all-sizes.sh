#!/usr/bin/env bash
ARGS=`cut -d' ' -f1,7 < ./plots/hospitals-stats.txt | paste -sd " " -`
echo "$ARGS" | xargs -n 2 -P 10 ./draw-hospital-size.sh
