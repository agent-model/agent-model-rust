#!/usr/bin/env bash
mkdir -p ./plots/
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_hospitals_stats -- \
--processed ../data/processed.json                     \
--output ./plots/hospitals-stats.txt
