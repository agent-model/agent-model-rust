#!/usr/bin/gnuplot -persist
set terminal pngcairo linewidth 3 enhanced font "arial,30" fontscale 1.0 size 1600,1000; set zeroaxis;;
set output "./plots/sizes-total.png"
set xlabel "Simulation step"
set ylabel "Number of agents"
set title "Total number of agents in hospitals"
set xrange [1000:1730]
set yrange [0:*]
set grid xtics linewidth 1
set xtics 56
plot "./plots/sizes-total.txt" with lines notitle
