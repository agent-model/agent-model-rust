#!/usr/bin/gnuplot -c
dataname = "./plots/sizes/sizeof-".ARG2.".txt"
imgname = "./plots/sizes/sizeof-".ARG2.".png"
set terminal pngcairo linewidth 3 enhanced font "arial,30" fontscale 1.0 size 1600,1000;
set output imgname
set autoscale xfix
set yrange [0:*]
set xtics 730
set style fill transparent solid 1 noborder
set xlabel "Simulation step"
set ylabel "Number of agents"
set title sprintf("Population no. %i", ARG1 + 1)
plot dataname with lines notitle
