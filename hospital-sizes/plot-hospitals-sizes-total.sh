#!/usr/bin/env bash
paste -d' ' plots/sizes/sizeof-*.txt | \
awk '{for(x=1;x<=NF;x++)if(x % 2 == 0)printf "%s", $x (x == NF || x == (NF-1)?"\n":" ")}' > ./plots/sizes-merged.txt
awk '{for(i=1;i<=NF;i++) j+=$i; print j; j=0 }' < ./plots/sizes-merged.txt > ./plots/sizes-total.txt
