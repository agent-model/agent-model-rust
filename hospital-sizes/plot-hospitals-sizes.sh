#!/usr/bin/env bash
mkdir -p ./plots/sizes/
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin plot_hospitals_sizes --  \
--processed ../data/processed.json                      \
--output ./plots/sizes/sizeof                           \
--hospitals "$@"
