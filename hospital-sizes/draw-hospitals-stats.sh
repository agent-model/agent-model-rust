#!/usr/bin/gnuplot -persist
set terminal pngcairo linewidth 3 enhanced font "arial,40" fontscale 1.0 size 3200,2000; set zeroaxis;;
set output "./plots/hospitals-stats.png"
set xlabel "Ordinal"
set ylabel "Number of agents"
set title "Populations"
set autoscale xfix
set offsets 0, 1, 0, 0
set key on
set key right top
set bars 1.0
set style fill empty
plot "./plots/hospitals-stats.txt" using 1:3:2:6:5 with candlesticks title 'quartiles' whiskerbars, \
    ''         using 1:4:4:4:4 with candlesticks lt -1 notitle, \
    ''         using 1:4 with points pt 13 title "median"
