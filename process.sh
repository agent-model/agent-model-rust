#!/usr/bin/env bash
DATA=`ls ./data | grep '.csv'`
RUSTFLAGS='-C target-cpu=native' cargo run --release --bin process -- \
--input "./data/$DATA"                                                     \
--output ./data/processed.json
